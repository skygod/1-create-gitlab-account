# Branching

Branches offer possibility to work on changes in parallel. It is a common concept among versioning tools and [essential for git](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging). Although you are free in naming branches and use them for your specific needs, often git repositories are organised after a certain branching model called [git flow](https://datasift.github.io/gitflow/IntroducingGitFlow.html). We recommend starting with git flow branching model and adapt according to your needs. Althouhg widely accepted, it was inveted in a era before continous deployment. In the following we briefly cover the branch types:

## master branch

There can only be one `master` branch. All changes will merged to master a some point. **There always is a develop branch**

1. **Recommendation: Never commit to master directly. Only push changes through merge requests**
1. **Recommendation: Restrict in gitlab commits to master through repository settings**
1. **Recommendation: In Gitlab, mark *master* as *protected* in repository settings, for not deleting this branch by accident**

## develop branch

`develop` branch collects all changes that are not meant for a particular release. **There always is a develop branch**

1. **Recommendation: If your ci process is efficient, you might want to consider restricting direct commits to develop, too (as you do for master).**
1. **Recommendation: In Gitlab, mark *develop* as *protected* in repository settings, for not deleting this branch by accident**

## feature branch

A `feature` branch is a temporary branch holding changes for a certain *feature* until it is completed. A *feature* may be an actual software feature, a component or even just a ticket. Feature branches often labeled `feature/<ticket-number>-<short-name>`, for it makes it easier for teams to quickly identify the use of a branch. 

## release and hotfix branch

`Release` branch is branched from develop collecting all changes for a certain release. Once the release is *ready* it is merged to master and tagged with version number. The actual artifacts may be built from that version tag. Afterwards release branch is merged in to develop, too. In a third step the release branch is deleted. This whole process is often called "closing release branch".

`Hotfix` branches are handled identically as release, but happen sudden. You do not *plan* a hotfix as you do with releases.

### Branches in continuous deployment

The concepts of `release` and `hotfix` branches are bit outdated. If you are working with continuous deployment, you might not use any of it, since you manage each commit to master as release. You would not need to collect certain changes in predefined *release*. However, if you are working with fixed release schedules, this concept is very reliable.

# Merge

*Merging* describes the merge of one branch in to another. Git is very tolerant when it comes to merge conflicts, provided changes only happen in text files and in independent lines. When merging locally, you always merge another branch in to the one currently checked out.

## Merge Request

Also known as *Pull Request*. A Merge Request is an organised merge process in Gitlab (called *Pull Request* in Github). A Merge Request always awaits approval. Only when the request is approved, the merge is executed.

The approval can be an actual person making reviews. Approval can also come from passed pipelines and thus be automated. With automated merge requests, you can approve requests under the condition *Merge when pipeline succeeds*. That means you do not need wait for a pipeline to end, but can focus on new topics, while Gitlab executes your merge automatically, given the underlying pipeline shows green.

Reference: [git-merge](https://git-scm.com/docs/git-merge)
Reference: [Gitlab Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/)
Reference: [Github Pull Requests](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/about-pull-requests)
